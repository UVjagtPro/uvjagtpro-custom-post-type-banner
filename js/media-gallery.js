jQuery(document).ready(function ($) 
{
  // Instantiates the variable that holds the media library frame.
  var meta_image_frame_large;
  var meta_image_frame;
  
  // Runs when the image button is clicked.
  $('.image-upload-large').click(function (e) 
  {
  
    // Get preview pane
    var meta_image_preview_large = $(this).parent().parent().children('.image-preview-large');
    
    // Prevents the default action from occuring.
    e.preventDefault();
    
    var meta_image_large = $(this).parent().children('.meta-image-large');
    
    // If the frame already exists, re-open it.
    if (meta_image_frame_large) 
    {
      meta_image_frame_large.open();
      return;
    }
    
    // Sets up the media library frame
    meta_image_frame_large = wp.media.frames.meta_image_frame_large = wp.media(
    {
      title: meta_image_large.title,
      button: {
        text: meta_image_large.button
      }
    });
    
    // Runs when an image is selected.
    meta_image_frame_large.on('select', function () 
    {
    
      // Grabs the attachment selection and creates a JSON representation of the model.
      var media_attachment = meta_image_frame_large.state().get('selection').first().toJSON();
      
      // Sends the attachment URL to our custom image input field.
      meta_image_large.val(media_attachment.url);
      meta_image_preview_large.children('img').attr('src', media_attachment.url);
    
    });

    // Opens the media library frame.
    meta_image_frame_large.open();
  
  });

  // Runs when the image button is clicked.
  $('.image-upload-small').click(function (e) 
  {
  
    // Get preview pane
    var meta_image_preview_small = $(this).parent().parent().children('.image-preview-small');
    
    // Prevents the default action from occuring.
    e.preventDefault();
    
    var meta_image_small = $(this).parent().children('.meta-image-small');
    
    // If the frame already exists, re-open it.
    if (meta_image_frame) 
    {
      meta_image_frame.open();
      return;
    }
    
    // Sets up the media library frame
    meta_image_frame = wp.media.frames.meta_image_frame = wp.media(
    {
      title: meta_image_small.title,
      button: {
        text: meta_image_small.button
      }
    });
    
    // Runs when an image is selected.
    meta_image_frame.on('select', function () 
    {
    
      // Grabs the attachment selection and creates a JSON representation of the model.
      var media_attachment = meta_image_frame.state().get('selection').first().toJSON();
      
      // Sends the attachment URL to our custom image input field.
      meta_image_small.val(media_attachment.url);
      meta_image_preview_small.children('img').attr('src', media_attachment.url);
    
    });

    // Opens the media library frame.
    meta_image_frame.open();
  
  });



  // Runs when the image button is clicked.
  $('.link-target').click(function (e) 
  {
    e.preventDefault();

    wpLink.open('example_209490');
    
    return false;
  });

  $('#wp-link-submit').on('click', function(event) 
  {
    var linkAtts = wpLink.getAttrs(); // The links attributes (href, target) are stored in an object, which can be access via  wpLink.getAttrs()

    $('.cmb_text_link').val(linkAtts.href); // Get the href attribute and add to a textfield, or use as you see fit

    wpLink.textarea = $('.cmb_text_link');  // To close the link dialogue, it is again expecting an wp_editor instance, so you need to give it something to set focus back to.
    
    wpLink.close(); // Close the dialogue
    
    event.preventDefault ? event.preventDefault() : event.returnValue = false;  // Trap any events
    
    event.stopPropagation();  // Trap any events
    
    return false;
  });

  $('#wp-link-cancel').on('click', function(event) 
  {
    wpLink.textarea = $('.cmb_text_link');
  
    wpLink.close();
    event.preventDefault ? event.preventDefault() : event.returnValue = false;    // Trap any events
    event.stopPropagation();  // Trap any events
    return false;
  });


});