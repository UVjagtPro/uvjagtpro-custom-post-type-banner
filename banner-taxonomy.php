<?php
	
	function bannner_taxonomy() 
	{

		register_taxonomy(
			'banner-category',
			'banner',
			array(
				'label' => __( 'Kategori' ),
				'rewrite' => array( 'slug' => 'banner-category' ),
				'hierarchical' => true,
			)
		);
	}

	add_action( 'init', 'bannner_taxonomy' );

?>