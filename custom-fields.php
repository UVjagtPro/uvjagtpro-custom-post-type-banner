<?php

	function add_your_fields_meta_box() 
	{

		add_meta_box(
			'your_fields_meta_box', // $id
			'Your Fields', // $title
			'show_your_fields_meta_box', // $callback
			'banner', // $screen
			'normal', // $context
			'high' // $priority
		);
	}

	add_action( 'add_meta_boxes', 'add_your_fields_meta_box' );

	/*######################################################################################################
	########################################################################################################
	########################################################################################################
	########################################################################################################
	############################### Enable custom meta boxes  ##############################################
	########################################################################################################
	########################################################################################################
	########################################################################################################
	########################################################################################################*/

	function show_your_fields_meta_box() 
	{
	
		global $post;  
	
		$meta = get_post_meta( $post->ID, 'your_fields', true ); ?>

		<input type="hidden" name="your_meta_box_nonce" value="<?php echo wp_create_nonce( basename(__FILE__) ); ?>">

    	<!-- All fields will go here -->

    	<p>
			<label for="your_fields[headline]">Overskrift</label>
			<br>
			<input type="text" name="your_fields[headline]" id="your_fields[headline]" class="regular-text" value="<?php echo $meta['headline']; ?>">
		</p>

		<p>
			<label for="your_fields[subheadline]">Supplerende tekst</label>
			<br>
			<textarea name="your_fields[subheadline]" id="your_fields[subheadline]" rows="5" cols="30" style="width:500px;"><?php echo $meta['subheadline']; ?></textarea>
		</p>

		<p>
			<label for="your_fields[large_image]">Stort billede</label>
			<br>
			<input type="text" name="your_fields[large_image]" id="your_fields[large_image]" class="meta-image-large regular-text" value="<?php echo $meta['large_image']; ?>">
			<input type="button" class="button image-upload-large" value="Browse">
		</p>

		<div class="image-preview-large"><img src="<?php echo $meta['large_image']; ?>" style="max-width: 250px;"></div>	

		<p>
			<label for="your_fields[small_image]">Lille billede</label>
			<br>
			<input type="text" name="your_fields[small_image]" id="your_fields[small_image]" class="meta-image-small regular-text" value="<?php echo $meta['small_image']; ?>">
			<input type="button" class="button image-upload-small" value="Browse">
		</p>

		<div class="image-preview-small"><img src="<?php echo $meta['small_image']; ?>" style="max-width: 250px;"></div>

		<p>
			<label for="your_fields[link_target]">Link</label>
			<br>
			<input type="text" name="your_fields[link_target]" id="your_fields[link_target]" class="regular-text" value="<?php echo $meta['link_target']; ?>">
		</p>

		<p>
			<label for="your_fields[button_text]">Tekst på knap</label>
			<br>
			<input type="text" name="your_fields[button_text]" id="your_fields[button_text]" class="regular-text" value="<?php echo $meta['button_text']; ?>">
		</p>

		<!--<p>
			<label for="your_fields[link_target]">Link target</label>
			<br>
			<input type="text" name="your_fields[link_target]" id="your_fields[link_target] example_209490" class="link-target regular-text" value="<?php echo $meta['link_target']; ?>">
			<input type="button" class="button link-target" value="Browse">
		</p>-->

	<?php }

	/*######################################################################################################
	########################################################################################################
	########################################################################################################
	########################################################################################################
	############################### Save meta box data to database #########################################
	########################################################################################################
	########################################################################################################
	########################################################################################################
	########################################################################################################*/

	function save_your_fields_meta( $post_id ) 
	{   

		// verify nonce
		if ( !wp_verify_nonce( $_POST['your_meta_box_nonce'], basename(__FILE__) ) ) 
		{
			return $post_id; 
		}
		
		// check autosave
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
			return $post_id;
		}
		
		// check permissions
		if ( 'page' === $_POST['post_type'] ) 
		{
			if ( !current_user_can( 'edit_page', $post_id ) ) 
			{
				
				return $post_id;

			} elseif ( !current_user_can( 'edit_post', $post_id ) ) {
				
				return $post_id;

			}  
		}
		
		$old = get_post_meta( $post_id, 'your_fields', true );
		$new = $_POST['your_fields'];

		if ( $new && $new !== $old ) 
		{
		
			update_post_meta( $post_id, 'your_fields', $new );
		
		} elseif ( '' === $new && $old ) {
			
			delete_post_meta( $post_id, 'your_fields', $old );
		
		}
	}
	
	add_action( 'save_post', 'save_your_fields_meta' );

?>