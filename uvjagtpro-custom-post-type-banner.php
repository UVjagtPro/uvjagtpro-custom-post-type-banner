<?php

	/**
	* Plugin Name: UVjagtPro - Custom post type (Forside banner)
	* Description: This plugin creates a custom post type named "Forside banner" when plugin is activated.
	* Author: Kim Nyegaard Andreasen
	* Version: 1.0
	*/

	/*######################################################################################################
	########################################################################################################
	########################################################################################################
	########################################################################################################
	############################### Include PHP scripts ####################################################
	########################################################################################################
	########################################################################################################
	########################################################################################################
	########################################################################################################*/

	include "banner-taxonomy.php";
	include "custom-post-type.php";
	include "custom-fields.php";

	/*######################################################################################################
	########################################################################################################
	########################################################################################################
	########################################################################################################
	############################### Enqueue media gallery script ###########################################
	########################################################################################################
	########################################################################################################
	########################################################################################################
	########################################################################################################*/

	function load_scripts() 
	{
		global $post;

	    wp_enqueue_media( 
	    	array( 
	    		'post' => $post->ID, 
	    	) 
	    );

	    // Custom scripts
		wp_enqueue_script( 'my-great-script', plugin_dir_url( __FILE__ ) . '/js/media-gallery.js', array( 'jquery' ), '1.0.0', true );
		//wp_enqueue_script( 'my-great-script', plugin_dir_url( __FILE__ ) . '/js/link-target.js', array( 'jquery' ), '1.0.0', true );

	    // Scripts out of the box
	    //wp_enqueue_script('wplink');
	    //wp_enqueue_style( 'editor-buttons' );
		//wp_enqueue_script('wpdialogs');
		//wp_enqueue_script('wpdialogs-popup');
		//wp_enqueue_style('wp-jquery-ui-dialog');
		//wp_enqueue_style('thickbox');
	}

	add_action( 'admin_enqueue_scripts', 'load_scripts' );

	/*######################################################################################################
	########################################################################################################
	########################################################################################################
	########################################################################################################
	############################### Query front page banner and display it #################################
	########################################################################################################
	########################################################################################################
	########################################################################################################
	########################################################################################################*/

	function home_page_banner() 
	{
	 
	    $args = array(
			'post_type' => 'banner',
	        'post_status' => 'publish',
	        'banner-category' => 'forside-banner-1',
	        'posts_per_page' => 1
		);  

		$your_loop = new WP_Query( $args ); 

		if( $your_loop->have_posts() ) : while ($your_loop->have_posts()) : $your_loop->the_post(); 
		
				$meta = get_post_meta( get_the_ID(), 'your_fields', true ); ?>
				
				<a href="<?php echo $meta['link_target']; ?>" target="_self">
					<div class="container">	
						<div id="large-image">
							<img src="<?php echo $meta['large_image']; ?>">
						</div>

						<div id="small-image">
							<img src="<?php echo $meta['small_image']; ?>">
						</div>

						<div class="banner-text">
							<h1><?php echo $meta['headline'];?></h1>				
							
							<?php 
							
								if ($meta['subheadline'] !== "") : 
								{
									?><h2><?php echo $meta['subheadline'];?></h2><?php
								} else : {
									// Nothing!!!	
								}
								endif;
							?>

							<div class="banner-button">
								<a href="<?php echo $meta['link_target']; ?>" target="_self"><?php echo $meta['button_text']; ?></a>
							</div>	
						</div>	
					</div>
				</a>

		<?php endwhile; wp_reset_postdata(); endif; 	 
	} 

	add_action( 'home_between_header_and_body', 'home_page_banner' );

	function home_page_banner_2() 
	{
	 
	    $args = array(
			'post_type' => 'banner',
	        'post_status' => 'publish',
	        'banner-category' => 'forside-banner-2',
	        'posts_per_page' => 1
		);  

		$your_loop = new WP_Query( $args ); 

		if( $your_loop->have_posts() ) : while ($your_loop->have_posts()) : $your_loop->the_post(); 
		
				$meta = get_post_meta( get_the_ID(), 'your_fields', true ); ?>
				
				<a href="<?php echo $meta['link_target']; ?>" target="_self">
					<div class="container">	
						<div id="large-image">
							<img src="<?php echo $meta['large_image']; ?>">
						</div>

						<div id="small-image">
							<img src="<?php echo $meta['small_image']; ?>">
						</div>

						<div class="banner-text">
							<h1><?php echo $meta['headline'];?></h1>				
							
							<?php 
							
								if ($meta['subheadline'] !== "") : 
								{
									?><h2><?php echo $meta['subheadline'];?></h2><?php
								} else : {
									// Nothing!!!	
								}
								endif;
							?>

							<div class="banner-button">
								<a href="<?php echo $meta['link_target']; ?>" target="_self"><?php echo $meta['button_text']; ?></a>
							</div>	
						</div>	
					</div>
				</a>

		<?php endwhile; wp_reset_postdata(); endif; 	 
	} 

	add_action( 'home_between_body_and_gooter', 'home_page_banner_2', 10 ); 

?>