<?php

	/*
	// register a custom post type called 'banner'*/

	function create_banner_post_type() 
	{
		
		$labels = array(
	        'name' 					=> __( 'Banner' ),
	        'singular_name' 		=> __( 'Banner' ),
	        'add_new' 				=> __( 'Nyt banner' ),
	        'add_new_item' 			=> __( 'Opret nyt banner' ),
	        'edit_item' 			=> __( 'Rediger banner' ),
	        'new_item' 				=> __( 'Nyt banner' ),
	        'view_item' 			=> __( 'Forhåndsvis banner' ),
	        'search_items' 			=> __( 'Søg bannere' ),
	        'not_found' 			=> __( 'Ingen bannere fundet' ),
	        'not_found_in_trash' 	=> __( 'Ingen bannere fundet i papirkurven' ),
	    );

	    $supports = array(
			'title',
			//'excerpt',
			//'editor',
			//'thumbnail',
			//'custom-fields',
			//'page-attributes'
			//'comments',
			//'revisions',
		);

		$taxonomies = array(
			//'post_tag', 
			'banner-category'
		);

		$args = array(
	        'labels' 		=> $labels,
	        'has_archive' 	=> true,
	        'public' 		=> true,
	        'hierarchical' 	=> false,
	        'taxonomies' 	=> $taxonomies,
	        'supports'      => $supports
	    );

		register_post_type( 'banner', $args );
	}
	
	add_action( 'init', 'create_banner_post_type' );
?>